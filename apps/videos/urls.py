from django.conf.urls import patterns, url

from videos.views import ListVideos, push_stat

urlpatterns = patterns('videos.views',
    url(r'^list_all/$', ListVideos.as_view(), name='videos_list'),
    url(r'^push_stat/$', push_stat, name='push_stat'),
)