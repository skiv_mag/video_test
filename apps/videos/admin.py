from django.contrib import admin
from videos.models import Video, VideoStat

# Register your models here.
admin.site.register(Video)
admin.site.register(VideoStat)
