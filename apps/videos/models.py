from django.db import models

# Create your models here.

class Video(models.Model):

    video_id = models.CharField(max_length=200, unique=True)

    def __unicode__(self):
        return "https://www.youtube.com/watch?v=%s" % self.video_id


class VideoStat(models.Model):
    video = models.ForeignKey(Video)
    loads = models.IntegerField(max_length=200, default=0)
    plays = models.IntegerField(max_length=200, default=0)

